<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    protected $table="properties";
    public function address(){
        return $this->hasOne("App\Address","address_id");
    }
    public function owners(){
        return $this->belongsToMany("App\Models\KimiyaUsers","owners","property_id","user_id");
    }
}
