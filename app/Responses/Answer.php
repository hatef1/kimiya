<?php


namespace App\Responses;


class Answer
{
    public $data;
    public $message;
    public $success;

    public function success($message = null,$data = []){
        $this->success = true;
        $this->message = $message;
        $this->data = $data;
    }
    public function fail($message = null,$data = []){
        $this->success = false;
        $this->message = $message;
        $this->data = $data;
    }
}
