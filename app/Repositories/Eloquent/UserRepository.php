<?php


namespace App\Repositories\Eloquent;


use App\Models\KimiyaUser;
use App\Models\Phone;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use App\Responses\Answer;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(User $user){
        $this->model = $user;
    }
    public function createUser($data): Answer
    {
        $answer = new Answer();
        $user= new KimiyaUser();
        $user->first_name = $data["first_name"];
        $user->last_name = $data["last_name"];
        if($user->save()){
            $numbers = explode(",",$data["phones"]);
            foreach ($numbers as $number){
                $splitNumber = explode(":",$number);
                $phone = new Phone();
                $phone->number = $splitNumber[0];
                $phone->type = $splitNumber[1];
                $phone->user_id = $user->id;
                $phone->save();
            }
            $answer->success("User has been created");
            return $answer;
        }

    }
}
