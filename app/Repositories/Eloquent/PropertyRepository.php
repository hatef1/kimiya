<?php


namespace App\Repositories\Eloquent;


use App\Models\Address;
use App\Models\KimiyaUser;
use App\Models\Phone;
use App\Models\Property;
use App\Models\User;
use App\Repositories\PropertyRepositoryInterface;
use App\Responses\Answer;


class PropertyRepository extends BaseRepository implements PropertyRepositoryInterface
{

    public function __construct(Property $property){
        $this->model = $property;
    }
    public function createProperty($data): Answer
    {
        $address = new Address();
        $address->address = $data["address"];
        $address->postal_code = $data["postal_code"];


        $address->save();

        $property= new Property();
        $property->address_id = $address->id;
        $property->save();

        $owners = explode(",", $data["owners"]);
        $property->owners()->sync($owners);

        $answer = new Answer();
        if($property){
            $answer->success("Property has been created",$property);
        }
        else{
            $answer->fail("Something went wrong");
        }
        return $answer;
    }

    public function updateProperty($data, $id): Answer
    {
        $answer = new Answer();
        $property= $this->model->find($id);

        $address = Address::where("id",$property->address_id)->first();

        $address->address = $data["address"];
        $address->postal_code = $data["postal_code"];

        $address->save();

        $owners = explode(",", $data["owners"]);
        $property->owners()->sync($owners);


        if($property){
            $answer->success("Property has been updated",$property);
        }
        else{
            $answer->fail("Something went wrong");
        }
        return $answer;
    }

    public function listProperty(): Answer
    {
        $answer= new Answer();
        $answer->success("Properties has been listed",$this->model->all());
        return $answer;
    }

    public function deleteProperty($id): Answer
    {
        $answer = new Answer();
        $property = $this->model->find($id);
        $property->owners()->sync([]);
        if(Address::where("id","=",$property->address_id)->delete()){
            if($property->delete()){
                $answer->success("Property has been deleted");
                return $answer;
            }
        }
    }

    public function singleProperty($id): Answer
    {
        $answer= new Answer();
        $property = $this->model->find($id);
        if($property){
            $answer->success("Property has been retrieved",$property);
        }
        else{
            $answer->fail("Something went wrong");
        }
        return $answer;
    }
}
