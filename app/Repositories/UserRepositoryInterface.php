<?php


namespace App\Repositories;


use App\Responses\Answer;

interface UserRepositoryInterface
{

    public function createUser($data):Answer;
}
