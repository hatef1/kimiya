<?php


namespace App\Repositories;


use App\Responses\Answer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface PropertyRepositoryInterface
{
    public function createProperty($data):Answer;
    public function updateProperty($data,$id):Answer;
    public function listProperty():Answer;
    public function deleteProperty($id):Answer;
    public function singleProperty($id):Answer;
}
