<?php


namespace App\Enums;


class PhoneType
{
    const HOME = "home";
    const WORK = "work";
    const MOBILE = "mobile";
}
