<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function create(Request $request, UserRepositoryInterface $userRepository){
        return response()->json($userRepository->createUser($request->all()));
    }
}
