<?php

namespace App\Http\Controllers;

use App\Repositories\PropertyRepositoryInterface;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    private $property;
    public function __construct(PropertyRepositoryInterface $property){
        $this->property = $property;
    }
    public function create(Request $request){
        $new_property = $this->property->createProperty($request->all());
        return response()->json($new_property);
    }
    public function update(Request $request,$id){
        $updated_property = $this->property->updateProperty($request->all(),$id);

        return response()->json($updated_property);
    }
    public function delete(Request $request,$id){
        $deleted_property = $this->property->deleteProperty($id);

        return response()->json($deleted_property);
    }
    public function index(Request $request){
        return response()->json($this->property->listProperty());
    }
    public function single(Request $request,$id){
        return response()->json($this->property->singleProperty($id));
    }
}
