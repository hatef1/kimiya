<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\PropertyController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("property")->group(function(){
    Route::get("/",[PropertyController::class,"index"]);
    Route::post("/",[PropertyController::class,"create"]);
    Route::get("/{id}",[PropertyController::class,"single"]);
    Route::post("/{id}",[PropertyController::class,"update"]);
    Route::delete("/{id}",[PropertyController::class,"delete"]);
});
Route::prefix("user")->group(function(){
    Route::post("/",[UserController::class,"create"]);
});
